import React, { useState, useEffect } from "react";

function ShoeForm () {
    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModel_Name] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');


    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value);
    }

    const handleModel_NameChange = (event) => {
        setModel_Name(event.target.value);
    }

    const handleColorChange = (event) => {
        setColor(event.target.value);
    }

    const handlePictureUrlChange = (event) => {
        setPictureUrl(event.target.value);
    }

    const hadleBinChange = (event) => {
        setBin(event.target.value);
    }

    const handleSubmit = async (event) => {
        setBin(event.target.value);
        const data = {};

        data.manufacturer = manufacturer;
        data.model_name = model_name;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin;
    };

    const shoeUrl = 'http://localhost:8000/api/shoes/';

    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
        const newShoe = await response.json();
        console.log(newShoe);

        setManufacturer('');
        setModel_Name('');
        setColor('');
        setPictureUrl('');
        setBin('');
    };

    async function fetchBins() {
        const binUrl = 'http://localhost:8100/api/sbins/';

        const response = await fetch(binUrl);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        } else {
            console.error('Error occured when fetching bin data')
        }
    };

    useEffect(() => {
        fetchBins();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit}
            id="create-shoe-form">

              <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange}
                placeholder="Manufacturer"
                required type="text"
                name="manufacturer"
                id="manufacturer"
                value={manufacturer}
                className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleModel_NameChange}
                placeholder="Model_Name"
                required type="text"
                name="model_name"
                id="model_name"
                value={model_name}
                className="form-control" />
                <label htmlFor="model_name">Model Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleColorChange}
                placeholder="Color"
                required type="text"
                name="color"
                id="color"
                value={color}
                className="form-control" />
                <label htmlFor="color">Color</label>
              </div>

              <div className="mb-3">
                <label htmlFor="picture_url">Picture Url</label>
                <input onChange={handlePictureUrlChange}
                name="picture_url"
                id="picture_url"
                value={pictureUrl}
                className="form-control"></input>
              </div>

              <div className="mb-3">
                <select onChange={handleBinChange}
                required name="bin"
                id="bin"
                value={bin}
                className="form-select">
                    <option value=''>Choose Bin</option>
                    {bins.map(bin => {
                        return (
                            <option key={bin.href} value={bin.href}>
                                {bin.closet_name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
        </div>
    );

}

export default ShoeForm;
